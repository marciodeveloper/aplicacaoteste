package com.revolutionapp.aplicacaoteste;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import model.notification.NotificationBundleModelActivity;
import model.notification.NotificationBundleToast;
import util.MaskEditUtil;

public class NotificacaoBudleActivity extends AppCompatActivity implements View.OnClickListener{

    private NotificationBundleToast notificationBundleToast;
    private NotificationBundleModelActivity modelActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificacao_budle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        montarNotificationBundleToast();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.btEnviarID){
            Intent intent = new Intent(NotificacaoBudleActivity.this, ExibeNotificacaoBundleActivity.class);
            this.notificationBundleToast = new NotificationBundleToast();
            this.notificationBundleToast = this.notificationBundleToast.montarNotificacao(this.modelActivity);
            intent.putExtra(getString(R.string.chave_notification_putextra), this.notificationBundleToast);
            startActivity(intent);
        }

    }

    private void montarNotificationBundleToast(){
        this.modelActivity = new NotificationBundleModelActivity();
        this.modelActivity.setTxtNomeID((EditText) findViewById(R.id.txtNomeID));
        this.modelActivity.setTxtCpfID((EditText) findViewById(R.id.txtCpfID));
        this.modelActivity.getTxtCpfID().addTextChangedListener(MaskEditUtil.mask(this.modelActivity.getTxtCpfID(), MaskEditUtil.FORMAT_CPF));
        this.modelActivity.setCh1ID((CheckBox) findViewById(R.id.ch1ID));
        this.modelActivity.setCh2ID((CheckBox) findViewById(R.id.ch2ID));
        this.modelActivity.setCh3ID((CheckBox) findViewById(R.id.ch3ID));
        this.modelActivity.setCh4ID((CheckBox) findViewById(R.id.ch4ID));
        this.modelActivity.setRadioGroup((RadioGroup) findViewById(R.id.radioGroup));
        this.modelActivity.setRd1ID((RadioButton) findViewById(R.id.rd1ID));
        this.modelActivity.setRd2ID((RadioButton) findViewById(R.id.rd2ID));
        this.modelActivity.setRd3ID((RadioButton) findViewById(R.id.rd3ID));
        this.modelActivity.setBtEnviarID((Button) findViewById(R.id.btEnviarID));
        this.modelActivity.getBtEnviarID().setOnClickListener(this);
    }

}
