package com.revolutionapp.aplicacaoteste;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AulaAventoButtonActivity extends AppCompatActivity {

    private Button botaoEnviar2;
    private TextView txtEvento;
    private TextView edtNome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aula_avento_button);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtEvento = findViewById(R.id.textView3);
        edtNome = findViewById(R.id.edtNome);
        botaoEnviar2 = findViewById(R.id.button2);

        botaoEnviar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtEvento.setText(edtNome.getText().toString());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Exemplo aula 12/02/2018 Button Enviar, maneira basica de manipular evento(esta configurado no evento visual)
    public void chamarEvento(View view){
        Log.v("MainActivity", "Clicando no Botão");
    }
}
