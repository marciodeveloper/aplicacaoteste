package com.revolutionapp.aplicacaoteste;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class OnResult2Activity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtvResult;
    private TextView txtvResult2;
    private CheckBox cbContinueCalc;
    private RadioGroup radioGroupOnResult2;
    private RadioButton rdOnresultSoma2;
    private RadioButton rdOnResultSubt2;
    private RadioButton rdOnResultDiv2;
    private RadioButton rdOnResultMult2;
    private Button btnOnResultCalc2;
    private Double resultadoRequest;
    private String operacaoResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_result2);
        montarOnResult2();
        executaCalculo();
    }


    private void montarOnResult2(){
        this.txtvResult = (TextView) findViewById(R.id.txtvResult);
        this.txtvResult2 = (TextView) findViewById(R.id.txtvResult2);
        this.cbContinueCalc = (CheckBox) findViewById(R.id.cbContinueCalc);
        this.radioGroupOnResult2 = (RadioGroup) findViewById(R.id.radioGroupOnResult2);
        this.radioGroupOnResult2.setVisibility(View.INVISIBLE);
        this.rdOnResultMult2 = (RadioButton) findViewById(R.id.rdOnResultMult2);
        this.rdOnresultSoma2 = (RadioButton) findViewById(R.id.rdOnresultSoma2);
        this.rdOnResultSubt2 = (RadioButton) findViewById(R.id.rdOnResultSubt2);
        this.rdOnResultDiv2 = (RadioButton) findViewById(R.id.rdOnResultDiv2);
        this.btnOnResultCalc2 = (Button) findViewById(R.id.btnOnResultCalc2);
        this.btnOnResultCalc2.setOnClickListener(this);
        this.cbContinueCalc.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.btnOnResultCalc2){
            if(this.cbContinueCalc.isChecked()){
                Intent intent = new Intent(OnResult2Activity.this, OnResultActivity.class);
                intent.putExtra("response", this.resultadoRequest);
                verificaOp();
                intent.putExtra("responseOp", this.operacaoResponse);
                setResult(RESULT_OK,intent);
            }else{
                setResult(RESULT_CANCELED);
            }
            finish();
        }else if(id == R.id.cbContinueCalc){
            if(this.cbContinueCalc.isChecked()){
                this.radioGroupOnResult2.setVisibility(View.VISIBLE);
            }else{
                this.radioGroupOnResult2.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void executaCalculo(){
        Intent intentRequest = getIntent();
        this.resultadoRequest = (Double)intentRequest.getExtras().get("resultado");
        this.txtvResult2.setText(resultadoRequest.toString());
    }

    private void verificaOp(){
        if(this.rdOnresultSoma2.isChecked()){
            this.operacaoResponse = "soma";
        }else if(this.rdOnResultSubt2.isChecked()){
            this.operacaoResponse = "subtracao";
        }else if(this.rdOnResultMult2.isChecked()){
            this.operacaoResponse = "multiplicacao";
        }else if(this.rdOnResultDiv2.isChecked()){
            this.operacaoResponse = "devisao";
        }
    }
}
