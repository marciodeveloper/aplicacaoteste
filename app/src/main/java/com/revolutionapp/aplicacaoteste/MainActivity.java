package com.revolutionapp.aplicacaoteste;

import android.content.Intent;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import model.ButtonsTelaInicial;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ButtonsTelaInicial btnsTelaInicial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        montarButons();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent intent;
       switch (id){
           case R.id.a:
               intent = new Intent(this, AulaAventoButtonActivity.class);
               startActivity(intent);
               break;
           case R.id.b:
               intent = new Intent(this, DesafioIMCActivity.class);
               startActivity(intent);
               break;
           case R.id.C:
               intent = new Intent(this, NotificacaoBudleActivity.class);
               startActivity(intent);
               break;
           case R.id.D:
               intent = new Intent(this, OnResultActivity.class);
               startActivity(intent);
               break;
           case R.id.E:
               intent = new Intent(this, ListViewActivity.class);
               startActivity(intent);
               break;
           default:
       }
    }

    private void montarButons(){
        btnsTelaInicial = new ButtonsTelaInicial();
        btnsTelaInicial.setBtnAula01((Button)findViewById(R.id.a));
        btnsTelaInicial.setBtnDesafio((Button)findViewById(R.id.b));
        btnsTelaInicial.setBtnNotificacao((Button)findViewById(R.id.C));
        btnsTelaInicial.setBtnOnResult((Button)findViewById(R.id.D));
        btnsTelaInicial.setBtnViewList((Button) findViewById(R.id.E));
        btnsTelaInicial.getBtnAula01().setOnClickListener(this);
        btnsTelaInicial.getBtnDesafio().setOnClickListener(this);
        btnsTelaInicial.getBtnNotificacao().setOnClickListener(this);
        btnsTelaInicial.getBtnOnResult().setOnClickListener(this);
        btnsTelaInicial.getBtnViewList().setOnClickListener(this);
    }

}
