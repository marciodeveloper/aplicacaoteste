package com.revolutionapp.aplicacaoteste;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import model.Imc;

public class DesafioIMCActivity extends AppCompatActivity implements View.OnClickListener {

    private Imc imc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_desafio_imc);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        montarImc();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(this.imc.getTxtPeso().getText().length() > 0
                && this.imc.getTxtAltura().getText().length() > 0
                && this.imc.getTxtNome().getText().length() > 0){
            Float peso = Float.parseFloat(this.imc.getTxtPeso().getText().toString());
            Float altura = Float.parseFloat(this.imc.getTxtAltura().getText().toString());
            Float alturMultiplicada = altura * altura;
            Float resultadoImc = peso/alturMultiplicada;
            String resultadoFinal = this.imc.getTxtNome().getText().toString().concat(getString(R.string.msg_custon_imc)).concat(this.imc.resultado(resultadoImc));
            this.imc.getTxtResultado().setText(resultadoFinal);
        }else{
            Toast.makeText(this, getString(R.string.aviso_dados_imc), Toast.LENGTH_SHORT).show();
        }
    }

    private void montarImc(){
        this.imc = new Imc();
        this.imc.setTxtNome((EditText)findViewById(R.id.txtNome));
        this.imc.setTxtPeso((EditText)findViewById(R.id.txtPeso));
        this.imc.setTxtAltura((EditText)findViewById(R.id.txtAltura));
        this.imc.setTxtResultado((TextView)findViewById(R.id.txtResultado));
        this.imc.setButtonCalcular((Button)findViewById(R.id.buttonCalcular));
        this.imc.getButtonCalcular().setOnClickListener(this);
    }
}
