package com.revolutionapp.aplicacaoteste;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import model.Usuario;
import util.Constantes;

public class ListViewActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    List<Usuario> usuarios = new ArrayList<>();
    ListView listView1;
    FloatingActionButton floatbutton;
    int position;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.usuarios.add(new Usuario(1L, "Marcio", "064.550.174-39"));
        this.usuarios.add(new Usuario(2L, "Aline", "064.550.200-90"));
        this.usuarios.add(new Usuario(3L, "Jorge", "345.888.200-90"));

        this.listView1 = findViewById(R.id.listView1ID);
        this.listView1.setOnItemClickListener(this);
        this.floatbutton = findViewById(R.id.floatButton1);
        this.floatbutton.setOnClickListener(this);

        this.atualizaAdapter();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if(id == R.id.floatButton1){
            Intent intent = new Intent(ListViewActivity.this, ListView2Activity.class);
            startActivityForResult(intent, Constantes.ON_RESULT_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){

            Usuario user = (Usuario)data.getExtras().getSerializable("response");
            user.setId(this.usuarios.size()+1l);
            this.usuarios.add(user);

        }else if(resultCode == Constantes.ON_RESULT_EDIT){

            Usuario user = (Usuario)data.getExtras().getSerializable("userEditResponse");
            this.usuarios.get(this.position).setNome(user.getNome());
            this.usuarios.get(this.position).setCpf(user.getCpf());
            this.atualizaAdapter();
            Toast.makeText(this, "Usuario alterado com sucesso.", Toast.LENGTH_SHORT).show();

        }else if(resultCode == Constantes.ON_RESULT_DELETE){

            this.usuarios.remove(this.position);
            this.atualizaAdapter();
            Toast.makeText(this, "Usuario excluido com sucesso.", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        this.position = position;
        Usuario user = this.usuarios.get(position);
        Intent intent = new Intent(ListViewActivity.this, ListView2Activity.class);
        intent.putExtra("userEdit", user);
        startActivityForResult(intent, Constantes.ON_REQUEST_EDIT);
    }

    private void atualizaAdapter(){
        this.adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, this.usuarios);
        this.listView1.setAdapter(this.adapter);
    }
}
