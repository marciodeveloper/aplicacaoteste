package com.revolutionapp.aplicacaoteste;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import util.Constantes;

public class OnResultActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText txtNumOnResult;
    private EditText txtNumOnResult2;
    private RadioGroup radioGroupOnResult;
    private RadioButton rdOnresultSoma;
    private RadioButton rdOnResultSubt;
    private RadioButton rdOnResultDiv;
    private RadioButton rdOnResultMult;
    private Button btnOnResultCalc;
    private Double response;
    private String opResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_result);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        montarOnResult();
        this.opResponse = "";
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void montarOnResult(){
        this.txtNumOnResult = (EditText) findViewById(R.id.txtNumOnResult);
        this.txtNumOnResult2 = (EditText) findViewById(R.id.txtNumOnResult2);
        this.radioGroupOnResult = (RadioGroup) findViewById(R.id.radioGroupOnResult);
        this.rdOnresultSoma = (RadioButton) findViewById(R.id.rdOnresultSoma);
        this.rdOnResultSubt = (RadioButton) findViewById(R.id.rdOnResultSubt);
        this.rdOnResultDiv = (RadioButton) findViewById(R.id.rdOnResultDiv);
        this.rdOnResultMult = (RadioButton) findViewById(R.id.rdOnResultMult);
        this.btnOnResultCalc = (Button) findViewById(R.id.btnOnResultCalc);
        this.btnOnResultCalc.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.btnOnResultCalc){
            Double resultado = calcular();
            if(resultado == 0){
                Toast.makeText(this, "Preencha os campos, primeiro e segundo número", Toast.LENGTH_SHORT).show();
            }else{

                Intent intent = new Intent(OnResultActivity.this, OnResult2Activity.class);
                if(this.opResponse != "" && this.opResponse != null){
                    intent.putExtra("resultado", verificaOp(this.opResponse, resultado));
                }else{
                    intent.putExtra("resultado", calcular());
                }

                startActivityForResult(intent, Constantes.ON_RESULT_REQUEST);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            this.response = data.getExtras().getDouble("response");
            this.opResponse = data.getExtras().getString("responseOp");
        }
    }

    private Double calcular(){
        Double resultado = 0d;
        if(this.txtNumOnResult.getText().length() > 0
                && this.txtNumOnResult2.getText().length() > 0){
            Double numeroUm = Double.valueOf(this.txtNumOnResult.getText().toString());
            Double numeroDois = Double.valueOf(this.txtNumOnResult2.getText().toString());

            if(this.rdOnresultSoma.isChecked()){
                resultado = numeroUm + numeroDois;
            }else if(this.rdOnResultSubt.isChecked()){
                resultado = numeroUm - numeroDois;
            }else if(this.rdOnResultMult.isChecked()){
                resultado = numeroUm * numeroDois;
            }else if(this.rdOnResultDiv.isChecked()){
                resultado = numeroUm / numeroDois;
            }
        }
        return resultado;
    }

    private Double verificaOp(String opResponse, Double request){
        if(opResponse.equals("soma")){
            return this.response + request;
        }else if(opResponse.equals("subtracao")){
            return this.response - request;
        }else if(opResponse.equals("multiplicacao")){
            return this.response * request;
        }else if(opResponse.equals("devisao")){
            return this.response / request;
        }
        return 0d;
    }
}
