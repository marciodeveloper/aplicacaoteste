package com.revolutionapp.aplicacaoteste;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import model.notification.NotificationBundleToast;

public class ExibeNotificacaoBundleActivity extends AppCompatActivity {

    private NotificationBundleToast notificationBundleToast;
    private TextView nome;
    private TextView cpf;
    private TextView tecs;
    private TextView idade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exibe_notificacao_bundle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.pegarComponentesDaView();
        this.capturaPutExtraNotification();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void capturaPutExtraNotification() {

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            this.notificationBundleToast = (NotificationBundleToast)extras.getParcelable(getString(R.string.chave_notification_putextra));
            Toast.makeText(this, getString(R.string.bem_vindo)+this.notificationBundleToast.getTxtNomeID(), Toast.LENGTH_SHORT).show();
            this.nome.setText(this.notificationBundleToast.getTxtNomeID());
            this.cpf.setText(this.notificationBundleToast.getTxtCpfID());
            this.tecs.setText(this.notificationBundleToast.getTecnologias());
            this.idade.setText(this.notificationBundleToast.getIdade());
        }
    }

    private void pegarComponentesDaView(){
        this.nome = findViewById(R.id.txtExibeNomeID);
        this.cpf = findViewById(R.id.txtExibeCpfID);
        this.tecs = findViewById(R.id.txtExibeTecsID);
        this.idade = findViewById(R.id.txtExibeIdadeID);
    }
}
