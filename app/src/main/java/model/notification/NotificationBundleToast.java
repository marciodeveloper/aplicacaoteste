package model.notification;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

public class NotificationBundleToast implements Parcelable {


    private String txtNomeID;
    private String txtCpfID;
    private String tecnologias;
    private String idade;

    public NotificationBundleToast(){
    }

    public void setTxtNomeID(String txtNomeID) {
        this.txtNomeID = txtNomeID;
    }

    public String getTxtNomeID() {
        return txtNomeID;
    }

    public void setTxtCpfID(String txtCpfID) {
        this.txtCpfID = txtCpfID;
    }

    public String getTxtCpfID() {
        return txtCpfID;
    }

    public void setTecnologias(String tecnologias) {
        this.tecnologias = tecnologias;
    }

    public String getTecnologias() {
        return tecnologias;
    }

    public void setIdade(String idade) {
        this.idade = idade;
    }

    public String getIdade() {
        return idade;
    }

    public NotificationBundleToast(Parcel in) {
        this.txtNomeID = in.readString();
        this.txtCpfID = in.readString();
        this.tecnologias = in.readString();
        this.idade = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.txtNomeID);
        dest.writeString(this.txtCpfID);
        dest.writeString(this.tecnologias);
        dest.writeString(this.idade);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NotificationBundleToast> CREATOR = new Creator<NotificationBundleToast>() {
        @Override
        public NotificationBundleToast createFromParcel(Parcel in) {
            return new NotificationBundleToast(in);
        }

        @Override
        public NotificationBundleToast[] newArray(int size) {
            return new NotificationBundleToast[size];
        }
    };


    public NotificationBundleToast montarNotificacao(NotificationBundleModelActivity modelActivity){

        NotificationBundleToast notificationBundleToast = new NotificationBundleToast();
        notificationBundleToast.setTxtNomeID(modelActivity.getTxtNomeID().getText().toString());
        notificationBundleToast.setTxtCpfID(modelActivity.getTxtCpfID().getText().toString());

        String tecs = "";
        String idade = "";

        if(modelActivity.getCh1ID().isChecked()){
            tecs = tecs+" - "+modelActivity.getCh1ID().getText().toString();
        }
        if(modelActivity.getCh2ID().isChecked()){
            tecs = tecs+" - "+modelActivity.getCh2ID().getText().toString();
        }
        if(modelActivity.getCh3ID().isChecked()){
            tecs = tecs+" - "+modelActivity.getCh3ID().getText().toString();
        }
        if(modelActivity.getCh4ID().isChecked()){
            tecs = tecs+" - "+modelActivity.getCh4ID().getText().toString();
        }
        notificationBundleToast.setTecnologias(tecs);

        if(modelActivity.getRd1ID().isChecked()){
            idade = modelActivity.getRd1ID().getText().toString();
        }else if(modelActivity.getRd2ID().isChecked()){
            idade = modelActivity.getRd2ID().getText().toString();
        }else {
            idade = modelActivity.getRd3ID().getText().toString();
        }
        notificationBundleToast.setIdade(idade);

        return notificationBundleToast;
    }
}
