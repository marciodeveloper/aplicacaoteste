package model.notification;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class NotificationBundleModelActivity {

    private EditText txtNomeID;
    private EditText txtCpfID;
    private CheckBox ch1ID;
    private CheckBox ch2ID;
    private CheckBox ch3ID;
    private CheckBox ch4ID;
    private RadioGroup radioGroup;
    private RadioButton rd1ID;
    private RadioButton rd2ID;
    private RadioButton rd3ID;
    private Button btEnviarID;

    public EditText getTxtNomeID() {
        return txtNomeID;
    }

    public void setTxtNomeID(EditText txtNomeID) {
        this.txtNomeID = txtNomeID;
    }

    public EditText getTxtCpfID() {
        return txtCpfID;
    }

    public void setTxtCpfID(EditText txtCpfID) {
        this.txtCpfID = txtCpfID;
    }

    public CheckBox getCh1ID() {
        return ch1ID;
    }

    public void setCh1ID(CheckBox ch1ID) {
        this.ch1ID = ch1ID;
    }

    public CheckBox getCh2ID() {
        return ch2ID;
    }

    public void setCh2ID(CheckBox ch2ID) {
        this.ch2ID = ch2ID;
    }

    public CheckBox getCh3ID() {
        return ch3ID;
    }

    public void setCh3ID(CheckBox ch3ID) {
        this.ch3ID = ch3ID;
    }

    public CheckBox getCh4ID() {
        return ch4ID;
    }

    public void setCh4ID(CheckBox ch4ID) {
        this.ch4ID = ch4ID;
    }

    public RadioGroup getRadioGroup() {
        return radioGroup;
    }

    public void setRadioGroup(RadioGroup radioGroup) {
        this.radioGroup = radioGroup;
    }

    public RadioButton getRd1ID() {
        return rd1ID;
    }

    public void setRd1ID(RadioButton rd1ID) {
        this.rd1ID = rd1ID;
    }

    public RadioButton getRd2ID() {
        return rd2ID;
    }

    public void setRd2ID(RadioButton rd2ID) {
        this.rd2ID = rd2ID;
    }

    public RadioButton getRd3ID() {
        return rd3ID;
    }

    public void setRd3ID(RadioButton rd3ID) {
        this.rd3ID = rd3ID;
    }

    public Button getBtEnviarID() {
        return btEnviarID;
    }

    public void setBtEnviarID(Button btEnviarID) {
        this.btEnviarID = btEnviarID;
    }
}
