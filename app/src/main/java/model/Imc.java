package model;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Imc {

    private static String MUITO_ABAIXO_DO_PESO = "Muito abaixo do peso";
    private static String ABAIXO_DO_PESO = "Abaixo do peso";
    private static String PESO_NORMAL = "Peso normal";
    private static String ACIMA_DO_PESO = "Acima do peso";
    private static String OBESIDADE_GRAU_I = "Obesidade Grau I";
    private static String OBESIDADE_GRAU_II = "Obesidade Grau II";
    private static String OBESIDADE_GRAU_III = "Obesidade Grau III";
    private EditText txtNome;
    private EditText txtPeso;
    private EditText txtAltura;
    private TextView txtResultado;
    private Button buttonCalcular;

    public void setTxtNome(EditText txtNome) {
        this.txtNome = txtNome;
    }

    public void setTxtPeso(EditText txtPeso) {
        this.txtPeso = txtPeso;
    }

    public void setTxtAltura(EditText txtAltura) {
        this.txtAltura = txtAltura;
    }

    public void setTxtResultado(TextView txtResultado) {
        this.txtResultado = txtResultado;
    }

    public void setButtonCalcular(Button buttonCalcular) {
        this.buttonCalcular = buttonCalcular;
    }

    public EditText getTxtNome() {
        return txtNome;
    }

    public EditText getTxtPeso() {
        return txtPeso;
    }

    public EditText getTxtAltura() {
        return txtAltura;
    }

    public TextView getTxtResultado() {
        return txtResultado;
    }

    public Button getButtonCalcular() {
        return buttonCalcular;
    }

    public String resultado(Float valor){
        if(valor < 17){
            return MUITO_ABAIXO_DO_PESO;
        } else if(valor >= 17 && valor <= 18.4){
            return ABAIXO_DO_PESO;
        } else if(valor >= 18.5 && valor <= 24.9){
            return PESO_NORMAL;
        } else if(valor >= 25 && valor <= 29.9){
            return ACIMA_DO_PESO;
        } else if(valor >= 30 && valor <= 34.9){
            return OBESIDADE_GRAU_I;
        } else if(valor >= 35 && valor <= 40){
            return OBESIDADE_GRAU_II;
        } else {
            return OBESIDADE_GRAU_III;
        }
    }
}
