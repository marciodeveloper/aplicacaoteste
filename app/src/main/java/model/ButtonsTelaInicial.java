package model;

import android.widget.Button;

public class ButtonsTelaInicial {

    private Button btnAula01;
    private Button btnDesafio;
    private Button btnNotificacao;
    private Button btnOnResult;
    private Button btnViewList;

    public void setBtnAula01(Button btnAula01) {
        this.btnAula01 = btnAula01;
    }

    public void setBtnDesafio(Button btnDesafio) {
        this.btnDesafio = btnDesafio;
    }

    public Button getBtnAula01() {
        return btnAula01;
    }

    public Button getBtnDesafio() {
        return btnDesafio;
    }

    public void setBtnNotificacao(Button btnNotificacao) {
        this.btnNotificacao = btnNotificacao;
    }

    public Button getBtnNotificacao() {
        return btnNotificacao;
    }

    public void setBtnOnResult(Button btnOnResult) {
        this.btnOnResult = btnOnResult;
    }

    public Button getBtnOnResult() {
        return btnOnResult;
    }

    public void setBtnViewList(Button btnViewList) {
        this.btnViewList = btnViewList;
    }

    public Button getBtnViewList() {
        return btnViewList;
    }
}
